import React, { useState } from 'react';
import M from 'materialize-css';

import api from '../../services/api';

import logo from '../../assets/logo-nav.png';
import './styles.scss';
import Enterprise from './Enterprise';

export default function Home({ history }) {
    const [showSearch, setShowSearch] = useState(false);
    const [search, setSearch] = useState('');
    const [enterprises, setEnterprises] = useState([]);
    const [loading, setLoading] = useState('');
    let searchInput = {};

    function toogleSearchNav() {
        setShowSearch(!showSearch);
        searchInput.focus();
    }

    function handleSearchChange(event) {
        const value = event.target.value;
        setSearch(value);
        searchEnterprise(value);
    }

    async function searchEnterprise(name) {
        try {
            if (name === '') {
                setEnterprises([]);
            }
            else {
                const config = {
                    params: { name },
                    headers: {
                        'access-token': localStorage.getItem('access-token'),
                        'client': localStorage.getItem('client'),
                        'uid': localStorage.getItem('uid')
                    }
                };

                setLoading(true);

                const { data } = await api.get('/enterprises', config);
                
                setLoading(false);
                setEnterprises(data.enterprises);
            }
        } catch (error) {
            console.error(error);
            M.toast({ html: 'Um erro ocorreu!' });
        }
    }

    return (
        <>
            <nav>
                <div className={`nav-wrapper ${showSearch ? 'show-search' : ''}`}>
                    <div id="logo-wrapper">
                        <a href="#" className="brand-logo center">
                            <img id="logo" src={logo} alt="ioasys" />
                        </a>
                        <ul className="right ">
                            <li><a href="#!" onClick={toogleSearchNav}><i className="material-icons left">search</i></a></li>
                        </ul>
                    </div>
                    <div className="input-field">
                        <input
                            id="search"
                            type="search"
                            placeholder="Pesquisar"
                            autoComplete="off"
                            value={search}
                            onChange={handleSearchChange}
                            ref={(input) => { searchInput = input; }}
                        />
                        <label className="label-icon" htmlFor="search">
                            <i className="material-icons">search</i>
                        </label>
                        <i className="material-icons" onClick={toogleSearchNav}>close</i>
                    </div>
                </div>
            </nav>

            <div className="default-container">
                {
                    enterprises.length === 0 &&
                    <div id="search-msg-wrapper">
                        {
                            loading ? <div>Carregando...</div> :
                            search === ''
                                ?
                                <div id="search-msg">Clique na busca para iniciar.</div>
                                :
                                <div id="not-found-msg">Nenhuma empresa foi encontrada para a busca realizada.</div>
                        }

                    </div>
                }


                {
                    enterprises
                        .map(({ id, enterprise_name, photo, country, enterprise_type }) => {
                            return <Enterprise
                                key={id}
                                id={id}
                                enterprise_name={enterprise_name}
                                photo={photo}
                                country={country}
                                enterprise_type_name={enterprise_type.enterprise_type_name}
                                history={history}
                            />
                        })
                }
            </div>
        </>
    )
}