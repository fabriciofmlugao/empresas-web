import React from 'react';
import './styles.scss';

export default function Enterprise({ id, enterprise_name, photo, country, enterprise_type_name, history }) {
    function handleEnterpriseClick() {
        history.push(`/EnterpriseDetail/${id}`);
    }

    return (
        <div id={id} className="card-panel enterprise waves-effect" onClick={handleEnterpriseClick}>
            <div className="enterprise-photo-wrapper">
                <img src={`https://empresas.ioasys.com.br/${photo}`} alt={enterprise_name}/>
            </div>
            <div className="enterprise-info-wrapper">
                <h2 className="enterprise-name">{enterprise_name}</h2>
                <div className="enterprise-type">{enterprise_type_name}</div>
                <div className="enterprise-country">{country}</div>
            </div>
        </div>
    )
}