import React, { useState } from 'react';

import api from '../../services/api';
import logo from '../../assets/logo-home.png';
import './styles.scss';


export default function Login({ history }) {
    const [dataState, setDataState] = useState({
        email: '',
        password: ''
    });
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    const [reviewPassword, setReviewPassword] = useState(false);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        setLoading(true);
        try {
            const response = await api.post('/users/auth/sign_in', data);

            localStorage.setItem('access-token', response.headers['access-token']);
            localStorage.setItem('client', response.headers['client']);
            localStorage.setItem('uid', response.headers['uid']);
            
            history.push('/home');
        } catch (error) {
            console.error(error)
            setError(true);
        }

        setLoading(false);
    }

    function togglePasswordVisibility() {
        setReviewPassword(!reviewPassword);
    }

    function handleChange({ target }) {
        if (error) {
            setError(false);
        }

        setDataState({
            ...dataState,
            [target.name]: target.value
        });
    }

    return (
        <div id="page-login">
            <div className={`loader ${loading ? 'active' : ''}`}>
                <div className="preloader-wrapper active">
                    <div className="spinner-layer">
                        <div className="circle-clipper left">
                            <div className="circle"></div>
                        </div><div className="gap-patch">
                            <div className="circle"></div>
                        </div><div className="circle-clipper right">
                            <div className="circle"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="login-container">
                <img id="logo" src={logo} alt="ioasys" />

                <div className="row">
                    <div className="col s12">
                        <h1 className="title">BEM-VINDO AO EMPRESAS</h1>
                    </div>
                    <div className="col s12">
                        <span className="description">Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</span>
                    </div>
                </div>

                <div className="row">
                    <form onSubmit={handleSubmit} className={error ? 'error' : ''}>
                        <div className="col s12 input-field">
                            <i className="material-icons-outlined prefix">email</i>
                            <div className="error-input"></div>
                            <input
                                type="text"
                                id="email"
                                name="email"
                                value={dataState.email}
                                placeholder="E-mail"
                                onChange={handleChange}
                            />
                        </div>

                        <div className="col s12 input-field">
                            <i className="material-icons-outlined prefix">lock_open</i>
                            <div className="error-input"></div>
                            <div className="review-password-wrapper" onClick={togglePasswordVisibility}>
                                {
                                    reviewPassword ?
                                    <i className="material-icons-outlined">visibility_off</i>
                                    :
                                    <i className="material-icons-outlined">visibility</i>
                                }
                                
                            </div>
                            <input
                                type={reviewPassword ? "text" : "password"}
                                id="password"
                                name="password"
                                placeholder="Senha"
                                value={dataState.password}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="col s12">
                            <div className="msg-error">Credenciais informadas são inválidas, tente novamente.</div>
                        </div>
                        <div className="col s12">
                            <button type="submit" className="btn btn-login waves-effect waves-light">Entrar</button>
                        </div>
                    </form>
                </div>


            </div>
        </div >
    )
}