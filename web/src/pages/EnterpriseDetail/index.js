import React, { useEffect, useState } from 'react';
import M from 'materialize-css';

import api from '../../services/api';

import './styles.scss';
import { Link } from 'react-router-dom';

export default function EnterpriseDetail({ history, match }) {
    const [enterprise, setEnterprise] = useState({});

    useEffect(function () {
        getEnterpriseInfo();
    }, []);

    async function getEnterpriseInfo() {
        try {
            const config = {
                headers: {
                    'access-token': localStorage.getItem('access-token'),
                    'client': localStorage.getItem('client'),
                    'uid': localStorage.getItem('uid')
                }
            };

            const { data } = await api.get(`/enterprises/${match.params.id}`, config);

            setEnterprise(data.enterprise);
        } catch (error) {
            console.error(error);
            M.toast({ html: 'Um erro ocorreu!' });
        }
    }

    function handleBackClick() {
        history.goBack();
    }

    return (
        <div id="detail">
            <nav>
                <div className="nav-wrapper">
                    <Link to="/home" className="brand-logo truncate">
                        <i className="material-icons back-icon">arrow_back</i>
                        {enterprise.enterprise_name}
                    </Link>
                </div>
            </nav>

            {
                Object.keys(enterprise).length === 0 ||
                <div className="default-container">
                    <div id="enterprise" className="card-panel">
                        <img className="photo" src={`https://empresas.ioasys.com.br/${enterprise.photo}`} alt={enterprise.enterprise_name} />
                        <p className="description">{enterprise.description}</p>
                    </div>
                </div>
            }


        </div>
    );
}