import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Login from './pages/Login';
import Home from './pages/Home';
import EnterpriseDetail from './pages/EnterpriseDetail';

export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login} />
                <Route path="/Home" component={Home} />
                <Route path="/EnterpriseDetail/:id" component={EnterpriseDetail} />
            </Switch>
        </BrowserRouter>
    );
}